# Image and Video Importer

## Summary
This script will recures a location (source) looking for image and video 
files, and automatically copy them to 'date organized (yyyy-mm-dd)'
Folders in the current user's windows profile.

Images and Videos are split into two seperate targets:
USERPROFILE\Images and USERPROFILE\Videos

## How to Use

1. Download the PowerShell script

2. Run script and you will be prompted for a location to import from

	.\organize.ps1

3. (or) Run the script, passing in the location as parameter

	.\organize.ps1 "C:\Users\jdoe\Dropbox\Camera Uploads\"
	
4. Once complete, you will see a summary, like the example below

	==================================================================
	
	Organization Summary:
	
	68 - Total Files Found
	
	16 - Video Files Organized
	
	15 - Image Files Organized
	
	==================================================================
