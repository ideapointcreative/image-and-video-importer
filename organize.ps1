$SourcePath=$args[0]
# User Prompt
Write-Output "
===========================================
Optional:
Script can be run by passing SOURCE as argument. e.g.
organize.ps1 r:\DCIM

--------------------------=======================
IMAGE AND VIDEO IMPORTER
==================================================================

This script will recures a location (source) looking for image and video 
files, and automatically copy them to 'date organized (yyyy-mm-dd)'
Folders in the current user's windows profile.

Example Run Commands
.\organize.ps1 "C:\Users\joeki\Dropbox\Camera Uploads\"


Images and Videos are split into two seperate targets:
USERPROFILE\Images and USERPROFILE\Videos
----------------------------------------
Current User Profile Location: " $env:USERPROFILE "

==================================================================
"
if ($SourcePath.Length -eq 0) {
	# If Source not passed as argument, then prompt
	$SourcePath = Read-Host -Prompt 'Enter the path to the root to import (AKA Source) (e.g. R:\)'
	if ($SourcePath.Length -eq 0) 	{ Write-Error "A source input is required." }
}

# Get the files which should be moved, without folders
#$files = Get-ChildItem 'C:\Users\jking\Pictures' -Recurse | where {!$_.PsIsContainer}
$files = Get-ChildItem $SourcePath -Recurse | where {!$_.PsIsContainer}
 
# List Files which will be moved
#$files
 
# Target Filder where files should be moved to. The script will automatically create a folder for the year and month.
$targetPath_img = $env:USERPROFILE + "\Pictures\"
$targetPath_vid = $env:USERPROFILE + "\Videos\"

$TotalFiles = $files.Count
$img_count = 0
$vid_count = 0
	
foreach ($file in $files)
{
	# Get year and Month of the file
	# I used LastWriteTime since this are synced files and the creation day will be the date when it was synced
	$year = $file.LastWriteTime.Year.ToString()
	$month = $file.LastWriteTime.Month.ToString()
	$day = $file.LastWriteTime.Day.ToString()

	 
	# Pad Month and Date If needed
	if ($month.Length -eq 1) 	{ $month = '0' + $month }	
	if ($day.Length -eq 1) 		{ $day = '0' + $day }	
	 
	# Determine File Type - Image or Video
	if (($file -like "*.mp4") -or ($file -like "*.avi") -or ($file -like "*.mov") -or ($file -like "*.mts")) {
		$targetPath = $targetPath_vid
		$vid_count = $vid_count + 1
	} elseif (($file -like "*.jpg") -or ($file -like "*.gif") -or ($file -like "*.raw") -or ($file -like "*.png") -or ($file -like "*.jpeg")) {
		$targetPath = $targetPath_img
		$img_count = $img_count + 1
	} else {
        $targetPath = ""
    }
    
    if ($targetPath.Length -gt 0) {
	    # Set Directory Path
	    $Directory = $targetPath + "\" + $year + "-" + $month + "-" + $day
	    # Create directory if it doesn't exsist
	    if (!(Test-Path $Directory)) { New-Item $directory -type directory }
	 
	    # Move File to new location
	    $file | Move-Item -Destination $Directory
	    #$file | Copy-Item -Destination $Directory
    }
}

Write-Output "
==================================================================
Organization Summary:
"$TotalFiles" - Total Files Found
"$vid_count" - Video Files Organized
"$img_count" - Image Files Organized
==================================================================
"

